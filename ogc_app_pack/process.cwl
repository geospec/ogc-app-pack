$graph:
- class: Workflow
  doc: TestDPSfunctionality
  id: run-dps-test
  inputs:
    input_file:
      default: ''
      doc: ''
      label: input_file
      type: String
  label: run-dps-test
  outputs: {}
  steps:
    process:
      in:
        input_file: input_file
      out: {}
      run: '#process'
- arguments: []
  baseCommand: dps-unit-test/run-test.sh
  class: CommandLineTool
  id: process
  inputs:
    input_file:
      inputBinding:
        position: 1
      type: String
  outputs: {}
  requirements:
    DockerRequirement:
      dockerPull: mas.maap-project.org:5000/root/ade-base-images/vanilla:latest
$namespaces:
  s: https://schema.org/
cwlVersion: v1.0
s:softwareVersion: 1.0.0
schemas:
- http://schema.org/version/9.0/schemaorg-current-http.rdf
