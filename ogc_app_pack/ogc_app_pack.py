'''
Generates the process.cwl
'''

import os
import yaml
import logging
import sys
import json

logger = logging.getLogger()

LOCAL_PATH = os.path.dirname(os.path.realpath(__file__))

class AppPackGenerator:

    def __init__(self):
        templatedir=os.path.join(LOCAL_PATH, 'templates')
        self.process_cwl = self._load_template(os.path.join(templatedir, 'process.cwl'))


    def _load_template(self, process_cwl_fname):
        try:
            with open(process_cwl_fname, 'r') as f:
                return yaml.safe_load(f)
        except Exception as e:
            logging.error("An error occurred: %s", str(e))


    def generate_process_cwl(self, data):
        #data = json.loads(data)

        # Handle overview info
        self.process_cwl['$graph'][0]['label'] = data['algo_name']
        self.process_cwl['$graph'][0]['id'] = data['algo_name']
        self.process_cwl['$graph'][0]['doc'] = data['description']

        # Handle inputs
        count = 1
        tmp_input_defs = {}
        tmp_input_mapping = {}
        tmp_input_binding = {}
        for i in data['inputs']:
            print(i)
            tmp_input_mapping[i['name']] = i['name']
            tmp_input_defs[i['name']] = {'type': 'String', 'label': i['name'], 'doc': '', 'default': ''}
            tmp_input_binding[i['name']] = {'type': 'String', 'inputBinding': {'position': count}}
            count += 1

        self.process_cwl['$graph'][0]['inputs'] = tmp_input_defs
        self.process_cwl['$graph'][0]['steps']['process']['in'] = tmp_input_mapping
        self.process_cwl['$graph'][1]['inputs'] = tmp_input_binding

        # Handle outputs
        tmp_output_defs = {}
        tmp_output_mapping = {}
        tmp_output_binding = {}

        if 'outputs' in data:
            for i in data['outputs']:
                tmp_input_mapping[i['name']] = i['name']
                tmp_output_defs[i['name']] = {'type': 'String', 'label': i['name'], 'doc': '', 'default': ''}
                tmp_output_binding[i['name']] = {'type': 'String', 'outputBinding': {'glob': '.'}}

        self.process_cwl['$graph'][0]['outputs'] = tmp_output_defs
        self.process_cwl['$graph'][0]['steps']['process']['out'] = tmp_output_mapping
        self.process_cwl['$graph'][1]['outputs'] = tmp_output_binding

        # Handle remaining fields
        self.process_cwl['$graph'][1]['requirements']['DockerRequirement']['dockerPull'] = data['docker_url']
        self.process_cwl['$graph'][1]['baseCommand'] = data['run_command']
        
        # Write process.cwl data to file
        with open('process.cwl', 'w', encoding='utf-8') as f:
            yaml.dump(self.process_cwl, f, default_flow_style=False)


