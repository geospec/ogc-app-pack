# OGC Application Package

This is the reference implementation for OGC application package generation. The input is a yaml file containing information. The output is the OGC-defined `process.cwl`. The script is invoked as follows:

`generate_ogc_app_pack.py input.yaml`

## Input Format
TBD

## Context

The original version of the app-pack-generator for the SISTER project may be found here. It was forked from Unity's app-pack-generator here. These implementations follow the older OGC recommendations to include stage-in/stage-out CWL as well as the application descriptor as part of the application package. The latest recommendation is to remove stage-in and stage-out CWL from the application package definition and make them ADES-specific. The application descriptor is no longer recommended. The original app-pack-generator also contains implementation-specific features that are not necessarily required for application package generation such as Docker image build and papermilled Jupyter Notebook parsing.

For more information, refer to the OGC documentation.


